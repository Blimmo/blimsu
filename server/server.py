import asyncio
import configparser
import json
import os
import traceback

import aiohttp
import osuapi

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), "config.ini"))


def formatReturn(code, message):
    return {"statusCode": code, "body": message}


def getTimeout():
    rtime = context.get_remaining_time_in_millis()
    return max(rtime / 1000 - 1, 0)


async def addBeatmapData(api, bh):
    """Retrieves data from the get_beatmaps endpoint of the osu api"""
    bs = await api.get_beatmaps(beatmap_hash=bh)
    if len(bs) == 0:
        raise aiohttp.HttpProcessingError(code=404, message="Beatmap hash not found")
    assert len(bs) == 1
    b = bs[0]

    # Basic map details
    out = {"artist": b.artist,
           "title": b.title,
           "difficulty": b.version,
           # More map details
           "ar": b.diff_approach,
           "od": b.diff_overall,
           "hp": b.diff_drain,
           "cs": b.diff_size,
           "stars": round(b.difficultyrating, 2),
           "bpm": round(b.bpm),
           "max_map_combo": b.max_combo,
           "status": b.approved.value,
           "beatmap_id": b.beatmap_id,
           "beatmapset_id": b.beatmapset_id,
           "total_length": b.total_length,
           }
    return out


async def addBestScoreData(api, player, bid, comp_data):
    """Retrieves data from the get_user_best endpoint of the osu api"""
    userbest = await api.get_user_best(player, limit=100)
    if len(userbest) == 0:
        raise aiohttp.HttpProcessingError(code=404, message="Player not found")
    out = {"prepp": 0,
           "apparentpp": 0}
    # pp from userbest
    found = False
    for i, s in enumerate(userbest):
        if not found:
            scomp = (s.count300, s.count100, s.count50, s.countmiss, s.maxcombo)
            if int(s.beatmap_id) == bid and scomp == comp_data:
                out["pp"] = round(s.pp, 2)
                out["pp_weighted"] = round(s.pp * 0.95 ** i, 2)
                found = True
            out["prepp"] += s.pp * 0.95 ** i
        out["apparentpp"] += s.pp * 0.95 ** i
    if not found:
        out = {"pp": "#",
               "pp_weighted": "#",
               "prepp": "#",
               "apparentpp": "#",
               }
    return out


async def addUserData(api, player):
    """Retrieves data from the get_user endpoint of the osu api"""
    users = await api.get_user(player)
    if len(users) == 0:
        raise aiohttp.HttpProcessingError(code=404, message="Player not found")
    user = users[0]
    return {"afterpp": user.pp_raw,
            "pp_rank": user.pp_rank}


async def addTillerinoData(session, bid, mods, fc, acc):
    """Retrieves data from the ppaddict api"""
    fmt = "https://api.tillerino.org/beatmapinfo?beatmapid={beatmap}&k={key}&mods={mods}&wait={wait}&acc=1.0"
    url = fmt.format(key=config["tillerino"]["key"], beatmap=bid,
                     mods=mods, wait=round(getTimeout() * 1000))
    if not fc and acc != 1.0:
        # not just ss
        url += "&acc={acc}".format(acc=acc)
    ret = await session.get(url)
    ret.raise_for_status()
    till = await ret.json()

    out = {}
    for p in till["ppForAcc"]["entry"]:
        if p["key"] == 1.0:
            out["pp_ss"] = round(p["value"], 2)
        elif p["key"] == acc:
            out["pp_nochoke"] = round(p["value"], 2)
    # special case where acc = 1.0
    if acc == 1.0:
        out["pp_nochoke"] = out["pp_ss"]

    return out


async def main(din, loop):
    async with aiohttp.ClientSession(loop=loop) as session:
        # Beatmap request
        api = osuapi.OsuApi(config["osuapi"]["key"], connector=osuapi.AHConnector(sess=session, loop=loop))
        dout = await asyncio.wait_for(addBeatmapData(api, din["beatmap_hash"]), getTimeout(), loop=loop)

        # User best and tillerino requests
        torun = {asyncio.ensure_future(addBestScoreData(api, din["player_name"],
                                                        dout["beatmap_id"],
                                                        tuple(din["comp_data"])
                                                        ), loop=loop): "osubest",
                 asyncio.ensure_future(addUserData(api, din["player_name"]
                                                   ), loop=loop): "osuuser",
                 }
        if not din["ss"]:
            # If not an ss, asking tillerino is necessary
            torun.update({asyncio.ensure_future(addTillerinoData(session, dout["beatmap_id"],
                                                                 din["mod_val"], din["fc"],
                                                                 din["acc"])): "tillerino"})
        done, pending = await asyncio.wait(torun, timeout=getTimeout(), loop=loop)
        for p in pending:
            if torun[p] == "osubest":
                dout.update({"pp": "?",
                             "pp_weighted": "?",
                             "prepp": "?",
                             "apparentpp": "?",
                             })
            elif torun[p] == "osuuser":
                dout.update({"afterpp": "?",
                             "pp_rank": "?",
                             })
            elif torun[p] == "tillerino":
                dout.update({"pp_nochoke": "?",
                             "pp_ss": "?",
                             })
        for d in done:
            dout.update(d.result())
    return dout


def handler(event, passedcontext):
    global context
    context = passedcontext
    # Load din from the request body
    try:
        din = json.loads(event["body"])
    except ValueError:
        print("Bad json: {}, exiting with 400".format(event["body"]))
        return formatReturn(400, "Invalid request body (not json)")
    print("Recieved: {}".format(din))

    # Check din holds all required keys
    requiredKeys = ("beatmap_hash", "player_name", "comp_data", "ss", "mod_val", "fc", "acc")
    keysThere = [key in din for key in requiredKeys]
    if not all(keysThere):
        missing = []
        for i, there in enumerate(keysThere):
            if not there:
                missing.append(requiredKeys[i])
        print("Missing keys: {}, exiting with 400".format(missing))
        return formatReturn(400, "Invalid request body (bad dict)\nMissing keys: {}".format(missing))

    loop = asyncio.new_event_loop()
    try:
        out = loop.run_until_complete(main(din, loop))
    except asyncio.TimeoutError:
        print("Osu api timed out, exiting with 504")
        return formatReturn(504, "Osu api timed out")
    ##    except osuapi.errors.HTTPError as e:
    ##        return formatReturn(e.code, "Reason: {}\nBody: {}".format(e.reason, e.body))
    # Propagate tillerinoapi errors, also handles osu api errors (cus they dont raise them properly)
    except aiohttp.HttpProcessingError as e:
        traceback.print_exc()
        return formatReturn(e.code, e.message)
    except:
        traceback.print_exc()
        return formatReturn(500, traceback.format_exc())

    print("Returning: {}".format(out))
    return formatReturn(200, json.dumps(out))
