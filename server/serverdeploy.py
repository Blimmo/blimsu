import boto3
import zipfile
import os
import site

zipname = "server.zip"
rootdir = site.getsitepackages()[1]
# Must be in site-packages
requiredmods = ["aiohttp", "async_timeout", "chardet", "multidict", "osuapi"]
# Must be in cwd
requiredfiles = ["config.ini", "server.py"]

with zipfile.ZipFile(zipname, mode="w", compression=zipfile.ZIP_DEFLATED) as f:
    for mod in requiredmods:
        for dirpath, dirnames, filenames in os.walk(os.path.join(rootdir, mod)):
            print(dirpath)
            for file in filenames:
                f.write(os.path.join(dirpath, file), os.path.join(dirpath[len(rootdir):], file))
    for file in requiredfiles:
        f.write(file)


client = boto3.client("lambda")

with open(zipname, "rb") as f:
    ret = client.update_function_code(
        FunctionName="blimsu",
        ZipFile=f.read(),
        DryRun=False
        )

print(ret["FunctionArn"])
