Blimsu! a better score summary for osu!
=======================================

This program creates an image similar to the image produced by osu! when you
finish a beatmap that you can share.

It aims to include more, and more useful information than the default osu! one.

## Installation
For installation you have 3 options:

### Installer
Simply download an installer 
[here](https://drive.google.com/drive/folders/0Bx-esFiz6L1YeUlBeUhERUZENmc?usp=sharing)
and run it.

### .exe without installation
Download a zip file from 
[here](https://drive.google.com/drive/folders/0Bx-esFiz6L1YVnFBVERKSWxNS28?usp=sharing)
and extract it somewhere.

You can now run the exe however you won't have convience functions such as
the right-click option for .osr files

### From source
Download the source from this repo.

You'll now need to install dependencies, you can do this with this command:
```
pip install -r requirements.txt
```

## Usage
If the program is run with no arguments (for example by double clicking it),
it will prompt you to select a replay to produce a summary of.

If the program is run with an argument, it must be the path of an osu replay.

If the program has been installed with the installer, there will be a
right-click option "Open with blimsu!" when osu replays are right-clicked.