import asyncio
import os
import pickle
import time


async def process(filepath, coro, args, kwargs, delay):
    st = time.perf_counter()
    if os.path.exists(filepath):
        # we have a cached version
        with open(filepath, "rb") as f:
            r = pickle.load(f)
    else:
        # get it normally and cache it
        r = await coro(*args, **kwargs)
        with open(filepath, "wb") as f:
            pickle.dump(r, f)
    et = time.perf_counter()
    # needs to take at least delay long for timeout tests etc.
    await asyncio.sleep(max(delay - (et - st), 0))
    return r
