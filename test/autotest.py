import filecmp
import os
import shutil

import tester


def main():
    rootdir = os.path.dirname(__file__)

    replays = ["fcnomod", "topchoke"]

    old = []
    new = []
    for i in replays:
        tester.test(clientargs=(os.path.join(rootdir, "data", i) + ".osr",
                                os.path.join(rootdir, "out", i) + ".png",
                                False))
        if os.path.exists(os.path.join(rootdir, "check", i) + ".png"):
            old.append(i + ".png")
        else:
            new.append(i + ".png")
            shutil.copy(os.path.join(rootdir, "out", i) + ".png", os.path.join(rootdir, "check"))

    match, mismatch, errors = filecmp.cmpfiles(os.path.join(rootdir, "out"),
                                               os.path.join(rootdir, "check"),
                                               old,
                                               shallow=False)
    print("\nSummary:")
    if new:
        print(" New images generated:\n  {}".format("\n  ".join(new)))
    if match:
        print(" Valid images generated:\n  {}".format("\n  ".join(match)))
    if mismatch:
        print(" Invalid images generated:\n  {}".format("\n  ".join(mismatch)))
    if errors:
        print(" Errors occurred when comparing:\n  {}".format("\n  ".join(errors)))


if __name__ == "__main__":
    main()
