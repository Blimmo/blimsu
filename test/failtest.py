import os

import tester


def main():
    rootdir = os.path.dirname(__file__)

    # invalid osr, not submitted, no player
    for i in ["invalid", "notsubmitted", "noplayer"]:
        tester.test(clientargs=(os.path.join(rootdir, "data", i + ".osr"), None, False))

    # timeouts
    delaykeys = ["user", "userbest", "beatmap", "ppaddict"]
    for k in delaykeys:
        tester.test(clientargs=(os.path.join(rootdir, "data", "topchoke.osr"), None, False),
                    delays={i: 15 if i == k else 0 for i in delaykeys})

    # till timeout with ss (no error, move to autotest?)
    tester.test(clientargs=(os.path.join(rootdir, "data", "ss.osr"), None, False),
                delays={i: 15 if i == "ppaddict" else 0 for i in delaykeys})


if __name__ == "__main__":
    main()
