import os

import aiohttp
from mock import process
from osuapi.connectors import AHConnector as CleanConnector
from osuapi.model import OsuMode
from osuapi.osu import OsuApi as CleanOsuApi

path = os.path.join(os.path.dirname(__file__), "osuapidata")
# Default; overwritten by tester when imported
delays = {"user": 0,
          "userbest": 0,
          "beatmap": 0,
          }


class OsuApi(CleanOsuApi):
    async def get_user(self, username, *, mode=OsuMode.osu):
        return await process(os.path.join(path, username), super().get_user,
                             (username,), {"mode": mode},
                             delays["user"])

    async def get_user_best(self, username, *, mode=OsuMode.osu, limit=50):
        return await process(os.path.join(path, username + "best"), super().get_user_best,
                             (username,), {"mode": mode, "limit": limit},
                             delays["userbest"])

    async def get_beatmaps(self, *, since=None, beatmapset_id=None, beatmap_id=None, username=None, mode=OsuMode.osu,
                           include_converted=False, beatmap_hash=None, limit=500):
        return await process(os.path.join(path, beatmap_hash), super().get_beatmaps, (),
                             {"since": since, "beatmapset_id": beatmapset_id,
                              "beatmap_id": beatmap_id, "username": username,
                              "mode": mode, "include_converted": include_converted,
                              "beatmap_hash": beatmap_hash, "limit": limit},
                             delays["beatmap"])


class AHConnector(CleanConnector):
    def __init__(self, sess=None, loop=None):
        super().__init__(sess=aiohttp.ClientSession(loop=loop))
