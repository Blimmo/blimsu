import os
import sys
import trace

import autotest
import failtest

rootdir = os.path.dirname(__file__)

tracer = trace.Trace(
    ignoredirs=[sys.prefix, sys.exec_prefix],
    ignoremods=["mockrequests", "mockosuapi", "mock", "mockaiohttp", "tester", "http.server", "autotest", "failtest"],
    trace=0,
    count=1)

tracer.run("""autotest.main()""")
tracer.run("""failtest.main()""")

r = tracer.results()
r.write_results(show_missing=True, coverdir=os.path.join(rootdir, "coverage"))
