import os
import urllib.parse

import aiohttp as cleanaiohttp
from mock import process

path = os.path.join(os.path.dirname(__file__), "aiohttpdata")
# Default; overwritten by tester when imported
delays = {"ppaddict": 0}


def getfilepath(req):
    params = urllib.parse.parse_qs(urllib.parse.urlparse(req).query, strict_parsing=True)
    return os.path.join(path, "b{beatmapid[0]}m{mods[0]}".format(**params))


async def retrieve(req):
    ret = await cleanaiohttp.get(req)
    ret.raise_for_status()
    return await ret.json()


class MockResponse():
    async def init(self, req):
        self.data = await process(os.path.join(path, getfilepath(req)), retrieve,
                                  (req,), {}, delays["ppaddict"])
        return self

    def raise_for_status(self):
        pass

    async def json(self):
        return self.data


class ClientSession():
    def __init__(self, loop=None):
        pass

    async def __aenter__(self):
        return self

    async def __aexit__(self, *_):
        pass

    async def get(self, url):
        return await MockResponse().init(url)


class HttpProcessingError(Exception):
    def __init__(self, *, code, message):
        self.code = code
        self.message = message
