import sys, os

rootdir = os.path.dirname(os.path.dirname(__file__))
sys.path.append(os.path.join(rootdir, "client"))
sys.path.append(os.path.join(rootdir, "server"))
os.chdir(os.path.join(rootdir, "client"))

import blimsu as client
import mockrequests
client.requests = mockrequests
mockrequests.doMock() # optional, only test osrs work

argvals = client.parseArgs()
client.main(argvals.osrpath, argvals.outpath, not argvals.noshow)
