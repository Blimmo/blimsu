import os
import sys

rootdir = os.path.dirname(os.path.dirname(__file__))
sys.path.append(os.path.join(rootdir, "client"))
sys.path.append(os.path.join(rootdir, "server"))
os.chdir(os.path.join(rootdir, "client"))


def test(*, localserver=True, localosuapi=True, localppaddict=True,
         delays=None, clientargs=None):
    import blimsu as client

    if localserver:
        import mockrequests
        client.requests = mockrequests

    if localosuapi or localppaddict:
        if not localserver:
            raise ValueError("Can only mock osuapi and/or ppaddict using local server")
        mockrequests.upstreamMock(localosuapi, localppaddict, delays)

    if clientargs is None:
        argvals = client.parseArgs()
        clientargs = (argvals.osrpath, argvals.outpath, not argvals.noshow)

    client.main(*clientargs)


if __name__ == "__main__":
    delays = {"user": 0,
              "userbest": 0,
              "beatmap": 0,
              "ppaddict": 0}
    test(localserver=True, localosuapi=False, localppaddict=False, delays=delays)
