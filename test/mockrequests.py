import json
import time

import server

timeout = 10


class HTTPError(Exception):
    pass


class MockContext():
    def __init__(self, timeout):
        self.ts = time.time()
        self.timeout = timeout

    def get_remaining_time_in_millis(self):
        return (self.timeout - (time.time() - self.ts)) * 1000


class MockResponse():
    def __init__(self, url, data):
        assert url == "https://rig0zmhwql.execute-api.eu-west-2.amazonaws.com/prod/blimsu"
        self.ret = server.handler({"body": data}, MockContext(timeout))

    @property
    def status_code(self):
        return self.ret["statusCode"]

    def raise_for_status(self):
        if self.ret["statusCode"] >= 400:
            raise HTTPError("Server error, code {}".format(self.ret["statusCode"]))

    def json(self):
        return json.loads(self.ret["body"])

    @property
    def text(self):
        return self.ret["body"]


def post(url, data=None):
    return MockResponse(url, data)


def upstreamMock(osu=True, pp=True, delays=None):
    if osu:
        import mockosuapi
        if delays is not None:
            mockosuapi.delays = delays
        server.osuapi = mockosuapi
    if pp:
        import mockaiohttp
        if delays is not None:
            mockaiohttp.delays = delays
        server.aiohttp = mockaiohttp
