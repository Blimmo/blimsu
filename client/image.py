import math
import os
import tempfile
import urllib.request
import warnings
import webbrowser

from PIL import Image, ImageDraw, ImageFont

coords = {"pp": (126, 77),
          "pp_weighted": (126, 105),
          "pp_gain": (126, 135),
          "pp_nochoke": (126, 165),
          "pp_ss": (126, 194),
          "300s": (318, 76),
          "100s": (318, 106),
          "50s": (318, 134),
          "misses": (318, 164),
          "acc": (318, 194),
          "max_combo": (536, 87),
          "max_map_combo": (536, 134),
          "combo_percent": (536, 194),
          }
scoords = {"ar": (658, 43),
           "od": (658, 69),
           "cs": (658, 95),
           "hp": (658, 123),
           "length": (658, 151),
           "bpm": (658, 180),
           "stars": (658, 204),
           }

GREEN = (99, 233, 40)
YELLOW = (217, 172, 71)

hue_range = 135
hue_offset = -135
sat_steps = 5
sat_step_size = 15
sat_offset = 20
lig_range = 40
lig_offset = 25

fontname = "segoeui.ttf"
lfont = ImageFont.truetype("segoeuil.ttf", size=20)
sfont = ImageFont.truetype("segoeuil.ttf", size=18)


def truncate(artist, track, diff, mods, aim, fontstring, sizebounds):
    fmt = "{} - {} [{}]"
    fargs = [artist, track, diff]
    if mods != "":
        fmt += " +{}"
        fargs += [mods]
    out = fmt.format(*fargs)

    size = max(sizebounds) + 1
    while size > min(sizebounds):
        size -= 1
        font = ImageFont.truetype(fontstring, size=size)
        if font.getsize(out)[0] <= aim:
            return out, font

    # can't get small enough by font size reduction, start truncating
    def measure(x):
        return font.getsize(x)[0]
    saim = aim - measure(mods)
    while measure("{}… -".format(fargs[0])) > saim // 3:
        fargs[0] = fargs[0][:-2] + "…"
        out = fmt.format(*fargs)
        if measure(out) <= aim:
            return out, font
    while measure(" {}… ".format(fargs[1])) > saim // 3:
        fargs[1] = fargs[1][:-2] + "…"
        out = fmt.format(*fargs)
        if measure(out) <= aim:
            return out, font
    while measure("[{}…]".format(fargs[2])) > saim // 3:
        fargs[2] = fargs[2][:-2] + "…"
        out = fmt.format(*fargs)
        if measure(out) <= aim:
            return out, font
    return out, font


class Summary:
    """Creates a summary image.
    url parameter is opened as a url and the file there is used as the background image
    """
    def __init__(self, url):
        with urllib.request.urlopen(url) as f, warnings.catch_warnings():
            # opens an image retrieved from the web; might be malicious
            warnings.simplefilter("error", Image.DecompressionBombWarning)
            self.img = Image.open(f).convert("RGBA")
        self.txt = Image.open(os.path.join("data", "blimsubase.png"))
        self.draw = ImageDraw.Draw(self.txt)

    def add(self, data):
        """Draw data onto the image"""
        # Beatmap Name Section
        line, font = truncate(data["artist"], data["title"], data["difficulty"], data["mods"], self.txt.width - 40,
                              fontname, (20, 25))
        self.draw.text((40, 0), line, (255, 255, 255), font=font)
        ccoords = coords.copy()
        if data["ss"]:
            del ccoords["pp_ss"]
        if data["fc"]:
            del ccoords["pp_nochoke"]
        for k in ccoords:
            self.draw.text(coords[k], str(data[k]), (255, 255, 255), font=lfont)
        for k in scoords:
            self.draw.text(scoords[k], str(data[k]), (255, 255, 255), font=sfont)
        # Player
        x = sfont.getsize(data["player"])[0]
        self.draw.text((895 - x, 226), data["player"], (255, 255, 255), font=sfont)
        # Status image
        if data["status"] <= 0:
            self.statimg = Image.open(os.path.join("data", "pending.png"))
        elif data["status"] == 1 or data["status"] == 2:
            self.statimg = Image.open(os.path.join("data", "ranked.png"))
        elif data["status"] == 3:
            self.statimg = Image.open(os.path.join("data", "qualified.png"))
        elif data["status"] == 4:
            self.statimg = Image.open(os.path.join("data", "loved.png"))
        else:
            raise ValueError("Invalid status")
        # Rank image
        percent300s = data["300s"] / data["notes"]
        if data["ss"]:
            if "HD" in data["mods"] or "FL" in data["mods"]:
                self.rankimg = Image.open(os.path.join("data", "ranking-xh.png"))
            else:
                self.rankimg = Image.open(os.path.join("data", "ranking-x.png"))
        elif data["misses"] == 0 and percent300s > 0.9 and data["50s"] / data["notes"] < 0.1:
            if "HD" in data["mods"] or "FL" in data["mods"]:
                self.rankimg = Image.open(os.path.join("data", "ranking-sh.png"))
            else:
                self.rankimg = Image.open(os.path.join("data", "ranking-s.png"))
        elif (data["misses"] == 0 and percent300s > 0.8) or percent300s > 0.9:
            self.rankimg = Image.open(os.path.join("data", "ranking-a.png"))
        elif (data["misses"] == 0 and percent300s > 0.7) or percent300s > 0.6:
            self.rankimg = Image.open(os.path.join("data", "ranking-b.png"))
        elif percent300s > 0.6:
            self.rankimg = Image.open(os.path.join("data", "ranking-c.png"))
        else:
            self.rankimg = Image.open(os.path.join("data", "ranking-d.png"))
        self.bar = Bar(600, 20)
        self.bar.fill(data["diff_data"])
        self.bar.parse_data(data["scores"], data["total_length"])

    def show(self, outpath, show):
        """Finally create the image, save it to outpath and display it (if show is True)"""
        img = Image.alpha_composite(self.img, self.txt)
        img.paste(self.statimg, box=(0, 0), mask=self.statimg)
        img.paste(self.rankimg, box=(716, 30), mask=self.rankimg)
        img.paste(self.bar.img, box=(5, 230), mask=self.bar.img)
        if outpath is None:
            outpath = os.path.join(tempfile.gettempdir(), "out.png")
        img.save(outpath)
        if show:
            webbrowser.open("file://" + outpath)


class Bar:
    """Creates a timeline of length and height specified"""
    def __init__(self, length, height):
        self.length = length
        self.height = height
        self.img = Image.new("RGBA", (length, height))
        self.draw = ImageDraw.Draw(self.img)
        self.draw.rectangle(((0, 5), (length, height - 5)), fill="grey")
        m = Image.open(os.path.join("data", "miss.png"))
        self.missimg = m.resize((int(m.size[0] / (m.size[1] / height)), height))
        s = Image.open(os.path.join("data", "sliderbreak.png"))
        self.sliderimg = s.resize((int(s.size[0] / (s.size[1] / height)), height))

    def add_line(self, pos, col):
        """Draws a line pos (0 to 1) along the line of colour col"""
        x = int(pos * self.length)
        self.draw.line([(x, 0), (x, self.height)], fill=col, width=1)

    def add_miss(self, pos):
        """Draws a miss image pos (0 to 1) along the line"""
        x = int(pos * self.length - (self.missimg.size[0] / 2))
        self.img.paste(self.missimg, box=(x, 0), mask=self.missimg)

    def add_sliderbreak(self, pos):
        """Draws a sliderbreak image pos (0 to 1) along the line"""
        x = int(pos * self.length - (self.sliderimg.size[0] / 2))
        self.img.paste(self.sliderimg, box=(x, 0), mask=self.sliderimg)

    def parse_data(self, scores, length):
        """Draws scores data from a dict of hit_objects, scores"""
        for i in scores["100s"]:
            self.add_line(i.time.total_seconds() / length, GREEN)
        for i in scores["50s"]:
            self.add_line(i.time.total_seconds() / length, YELLOW)
        for i in scores["misses"]:
            self.add_miss(i.time.total_seconds() / length)
        for i in scores["slider_breaks"]:
            self.add_sliderbreak(i.time.total_seconds() / length)

    def fill_line(self, x, difference, size):
        """Draws a line of hue varying according to difference
        and saturation / lightness varying according to size
        """
        hue = (int(difference * hue_range) + hue_offset) % 360
        sat = math.ceil(size * sat_steps) * sat_step_size + sat_offset
        lig = int(size * lig_range) + lig_offset
        self.draw.line(
            (x, 5, x, self.height - 5),
            fill="hsl({},{}%,{}%)".format(hue, sat, lig),
            width=1
        )

    def fill(self, data):
        """Draws difficulty data from a list of difference, size pairs"""
        # data is scaled difference, size pairs
        for i, (difference, size) in enumerate(data):
            self.fill_line(i, difference, size)
