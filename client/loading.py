import logging
import os
import shutil
import tempfile
import threading
import tkinter as tk
import tkinter.messagebox as mb
import tkinter.ttk as ttk
import time
import traceback
from datetime import datetime

barmax = 500
timefmt = "{:.0%}, {:.1f}s remaining"
tempdir = os.path.join(tempfile.gettempdir(), "blimsu")
logfile = os.path.join(tempdir, "log.txt")
timesfile = os.path.join(tempdir, "times")
filefmt = "%Y-%m-%dT%H-%M-%S.txt"


class ExpectedError(Exception):
    """Exception raised when the cause is known"""
    def __init__(self, sort):
        super().__init__()
        self.sort = sort


class Times:
    """Keeps track of how long each section takes to improve loading time estimates"""
    def __init__(self, path):
        self.path = path
        self.dict = {}
        self.total = 0
        try:
            f = open(path)
        except (IOError, OSError):
            self.dict = {}
            self.total = 10
            return
        try:
            for line in f:
                key, value = line.split(',')
                try:
                    value = float(value)
                except ValueError:
                    continue
                self.total += value
                self.dict[key] = value
        finally:
            f.close()

    def __enter__(self):
        return self

    def __getitem__(self, item):
        return self.dict.get(item, 1)

    def __setitem__(self, key, value):
        try:
            old = self.dict[key]
        except KeyError:
            new = value
        else:
            new = (old + value) / 2
        self.dict[key] = new

    def __exit__(self, exc_type, exc_val, exc_tb):
        with open(self.path, 'w') as f:
            for k, v in self.dict.items():
                f.write(str(k) + ',' + str(v) + '\n')


class Loading(tk.Frame):
    """Displays and advances the loading bar"""
    def __init__(self, parent):
        super().__init__(parent)
        self.root = parent
        self.times = Times(timesfile)
        self.totaltime = self.times.total
        self.last_start = None
        self.last_name = None
        self.interval = int(self.totaltime * (1000 / barmax))
        self.barVar = tk.DoubleVar()
        self.stopval = 0
        self.barVar.trace("w", self.increment)
        self.maxval = barmax
        self.bar = ttk.Progressbar(self, length="10c", variable=self.barVar, maximum=self.maxval)
        self.bar.grid(row=0, column=0, columnspan=2)
        self.lbl = tk.Label(self, text="")
        self.lbl.grid(row=1, column=0, sticky=tk.E)
        self.timelbl = tk.Label(self, anchor=tk.E, text=timefmt.format(0, float(self.totaltime)))
        self.timelbl.grid(row=1, column=1, sticky=tk.W + tk.E)
        self.pack()
        self.focus_force()

    def increment(self, *_):
        val = self.barVar.get()
        if val >= self.stopval - 1 or val >= self.maxval - (self.maxval / 100):
            self.bar.stop()
        self.timelbl.config(text=timefmt.format(val / self.maxval, self.totaltime * (1 - (val / self.maxval))))

    def start(self, name):
        """Start a named loading section"""
        cur_time = time.perf_counter()
        if self.last_start is not None:
            self.times[self.last_name] = cur_time - self.last_start
        self.last_name = name
        self.last_start = cur_time
        duration = self.times[name]
        self.bar.stop()
        self.barVar.set(self.stopval)
        self.lbl.config(text=name)
        logging.info(name)
        amount = (duration / self.totaltime) * self.maxval
        self.stopval += amount
        self.bar.start(interval=self.interval)

    def stop(self):
        """Stop the loading bar displaying and quit the window"""
        if self.last_start is not None:
            self.times[self.last_name] = time.perf_counter() - self.last_start
        self.barVar.set(self.maxval - 1)
        self.root.quit()

    def ask(self, msg):
        return mb.askokcancel(title="Missing fields", message=msg)

    def error(self, sort, logmsg):
        usrmsg = sort
        try:
            logging.error(logmsg)
            logging.shutdown()
            outfile = shutil.copy(logfile, os.path.join(tempdir, datetime.now().strftime(filefmt)))
            usrmsg = "{}\n\n Log file saved to {}".format(sort, outfile)
            self.bar.stop()
        finally:
            mb.showerror(title="Error", message=usrmsg)
            self.root.quit()


def test(app):
    app.start("Starting first long task", 1)
    time.sleep(4)
    app.start("Making request to server", 5)
    int("a")
    try:
        int("a")
    except ValueError:
        raise ExpectedError("Server error")
    time.sleep(5.5)


def program(target, app, *args):
    try:
        target(app, *args)
    except ExpectedError as e:
        cxt = e.__context__
        out = traceback.format_exception(type(cxt), cxt, cxt.__traceback__)
        out += ["({})".format(e.sort)]
        app.error(e.sort, "".join(out))
    except Exception:
        app.error("Unexpected error", traceback.format_exc())
    else:
        app.stop()


def main(target, args=(), level=logging.INFO):
    # make sure my tempdir exists
    if os.path.isfile(tempdir):
        os.remove(tempdir)
    if not os.path.exists(tempdir):
        os.mkdir(tempdir)
    logging.basicConfig(filename=logfile, filemode="w", style="{", level=level,
                        format="{asctime} {levelname} - {message}")
    root = tk.Tk()
    root.title('blimsu')
    logging.info('Starting')
    app = Loading(root)
    with app.times:
        progThread = threading.Thread(target=program, args=(target, app) + args)
        progThread.start()
        # bring to front
        root.lift()
        root.attributes('-topmost', True)
        root.after_idle(root.attributes, '-topmost', False)
        root.mainloop()
        progThread.join()  # wait for main program to finish
    root.destroy()


if __name__ == "__main__":
    main(test)
