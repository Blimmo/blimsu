import argparse
import hashlib
import json
import os
import sys
import tkinter as tk
import urllib.request
from tkinter import filedialog

import requests
import slider

import loading
from image import Summary
from loading import ExpectedError
from mod import mods, modnames

if getattr(sys, 'frozen', False):
    os.chdir(os.path.dirname(sys.executable))

lambdaurl = "https://rig0zmhwql.execute-api.eu-west-2.amazonaws.com/prod/blimsu"
imgurl = "https://assets.ppy.sh//beatmaps/{}/covers/cover.jpg"
beatmapurl = "https://osu.ppy.sh/osu/{}"

askfmt = "Some data was unable to be retrieved.\nThe missing data are:\n{}\nWould you like to continue generating the image anyway?"
msgs = {
    404: "This is likely because the replay you chose wasn't submitted.\nThis means no information is available for it so blimsu can't generate a summary\nIf you think you did download it from the osu servers then it's possible the replay is corrupted or the osu servers are playing up. Try reexporting the replay and/or waiting a bit",
    500: "Something went wrong on the server :(",
    504: "The osu servers are being super slow so required data couldn't be retrieved.\nRetrying will probably work but if you've tried more than a few times then waiting a while (a couple of hours) may be a good idea",
    }


def parseArgs():
    """Parse command line arguments. If osrpath is not supplied, asks the user for one with askopenfilename"""
    parser = argparse.ArgumentParser()
    parser.add_argument("osrpath", nargs="?", help="The path to the osr file to be parsed")
    parser.add_argument("outpath", nargs="?", help="The path in which the generated file will be placed")
    parser.add_argument("-s", "--noshow", action="store_true", help="Don't show the generated image")
    argvals = parser.parse_args()

    if argvals.osrpath is None:
        # Not in args; need to ask
        print("Choose an osr file...", flush=True)
        temp = tk.Tk()
        temp.withdraw()
        argvals.osrpath = filedialog.askopenfilename(parent=temp, title="Choose osr file")
        temp.destroy()

    return argvals


def addReplayData(r):
    """Extract data from the replay file.
    Returns one dict with information to use in the summary
    And another with info to use in the server request
    """
    acc = r.accuracy
    nomiss_acc = slider.utils.accuracy(r.count_300 + r.count_miss, r.count_100, r.count_50, 0)

    out = {"mods": "".join(modnames[mod] for mod in mods(r)),
           "max_combo": r.max_combo,
           "acc": "{:.2%}".format(acc),
           "300s": r.count_300,
           "100s": r.count_100,
           "50s": r.count_50,
           "misses": r.count_miss,
           "notes": r.count_300 + r.count_100 + r.count_50 + r.count_miss,
           "fc": r.full_combo,
           "ss": acc == 1.0,
           "player_name": r.player_name,
           }
    toUse = {"mod_val": sum(mod.value for mod in mods(r)),
             "beatmap_hash": r.beatmap_md5,
             "fc": r.full_combo,
             "ss": acc == 1.0,
             "player_name": r.player_name,
             "acc": nomiss_acc,
             "comp_data": (r.count_300, r.count_100, r.count_50, r.count_miss, r.max_combo),
             }
    return out, toUse


def addServerData(toUse):
    """Retrieve data from a server request.
    Uses the argument as the request payload.
    Returns the data retrieved and a list of data keys that couldn't be
    """
    req = requests.post(lambdaurl, data=json.dumps(toUse))
    try:
        req.raise_for_status()
    except requests.HTTPError:
        msg = "Server error ({})".format(req.status_code)
        if req.status_code != 500:
            msg += ": " + req.text
        msg += "\n\n" + msgs[req.status_code]
        raise ExpectedError(msg)
    d = req.json()

    # Check sometimes missing fields
    smissing = ["pp", "pp_weighted", "prepp", "afterpp", "apparentpp"]
    if not toUse["fc"]:
        smissing.append("pp_nochoke")
    if not toUse["ss"]:
        smissing.append("pp_ss")
    missing = []
    for k in smissing:
        if d[k] == "?":
            if k in ["prepp", "afterpp", "apparentpp"]:
                if "pp_gain" not in missing:
                    missing.append("pp_gain")
            else:
                missing.append(k)

    return d, missing


def addExtra(d):
    """Calculates additional data from existing data. Mutates the argument so returns None"""
    d["combo_percent"] = "{:.2%}".format(d["max_combo"] / d["max_map_combo"])
    d["player"] = "{player_name} (#{pp_rank}) {afterpp}pp".format(**d)
    d["length"] = "{}:{}".format(d["total_length"] // 60, d["total_length"] % 60)
    if d["afterpp"] == "#" or d["prepp"] == "#":
        d["pp_gain"] = "#"
    elif d["afterpp"] == "?" or d["prepp"] == "?":
        d["pp_gain"] = "?"
    else:
        bonuspp = d["afterpp"] - d["apparentpp"]
        underpp = d["afterpp"] - (d["prepp"] + d["pp_weighted"] + bonuspp)
        beforepp = (underpp / 0.95) + d["prepp"] + bonuspp
        d["pp_gain"] = round(d["afterpp"] - beforepp, 2)


def addBarData(d, r):
    """Adds score and difficulty data necessary to construct a timeline"""
    with urllib.request.urlopen(beatmapurl.format(d["beatmap_id"])) as f:
        odata = f.read()

    # Check beatmap downloaded against hash
    if hashlib.md5(odata).hexdigest() != r.beatmap_md5:
        raise ExpectedError("Couldn't retrieve beatmap")

    r.beatmap = slider.Beatmap.parse(odata.decode("utf-8-sig"))

    d["scores"] = r.hits

    raw_data = r.beatmap.smoothed_difficulty(0.5, 600)[1]

    # data is speed, aim pairs
    data = [(d[1] - d[0], d[0] + d[1]) for d in raw_data]
    # now it's difference, size pairs
    d["diff_data"] = scale_data(data)
    # now every number is between 0 and 1 (incl.)


def scale_data(data):
    """Scales data so that it all lies between 0 and 1"""
    maxs = tuple(map(max, *data))
    mins = tuple(map(min, *data))

    def scale(i, datum):
        return (datum - mins[i]) / (maxs[i] - mins[i])

    return [tuple(map(scale, *zip(*enumerate(pair)))) for pair in data]


def main(osrname, outname, show):
    loading.main(program, (osrname, outname, show))


def program(app, osrname, outname, show):
    app.start("Reading replay file")
    try:
        r = slider.Replay.from_path(osrname, retrieve_beatmap=False)
    except Exception:
        raise ExpectedError("Invalid replay")
    d, toUse = addReplayData(r)

    app.start("Getting data from server")
    sd, missing = addServerData(toUse)
    d.update(sd)
    if missing != [] and not app.ask(askfmt.format("\n".join(missing))):
        return

    app.start("Calculating extra data")
    addExtra(d)

    app.start("Calculating miss locations")
    addBarData(d, r)

    app.start("Retrieving background image")
    s = Summary(imgurl.format(d["beatmapset_id"]))
    app.start("Adding data to image")
    s.add(d)
    app.start("Displaying image")
    s.show(outname, show)


if __name__ == "__main__":
    argvals = parseArgs()
    main(argvals.osrpath, argvals.outpath, not argvals.noshow)
