import os
import platform
import shutil
import subprocess

import setup


def findBuildDir():
    for i in os.listdir(os.path.join(dirname, "build")):
        i = os.path.join(dirname, "build", i)
        if os.path.isdir(i):
            return i

arch = platform.architecture()[0]
installerName = "blimsu!installer{}.exe".format(arch)
zipName = "blimsu!{}.zip".format(arch)

dirname = os.path.dirname(__file__)

builddir = findBuildDir()
if builddir is not None:
    shutil.rmtree(builddir)

setup.main()

builddir = findBuildDir()

print("----- Deleting extra files -----")
for i in os.listdir(os.path.join(builddir, "tcl")):
    if os.path.isdir(os.path.join(builddir, "tcl", i)):
        shutil.rmtree(os.path.join(builddir, "tcl", i))
for i in os.listdir(os.path.join(builddir, "tk")):
    if os.path.isdir(os.path.join(builddir, "tk", i)) and i != "ttk":
        shutil.rmtree(os.path.join(builddir, "tk", i))
shutil.rmtree(os.path.join(builddir, "pytz", "zoneinfo"))

print("----- Creating sfx archive -----")
os.remove(r"{0}\build\{1}".format(dirname, installerName))
r = subprocess.run(r'C:\Program Files\WinRAR\WinRAR.exe a -afzip -ep1 -r -sfx -iadm -ibck -z"{0}\installer.conf" "{0}\build\{1}" "{2}\*"'.format(dirname, installerName, builddir))
r.check_returncode()
print("----- Creating normal archive -----")
os.remove(r"{0}\build\{1}".format(dirname, zipName))
r = subprocess.run(r'C:\Program Files\WinRAR\WinRAR.exe a -afzip -ep1 -r -ibck "{0}\build\{1}" "{2}\*"'.format(dirname, zipName, builddir))
r.check_returncode()
