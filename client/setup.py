import os.path
import platform
import sys
import site

from cx_Freeze import setup, Executable


def main():
    if len(sys.argv) == 1:
        sys.argv.append("build")

    arch = platform.architecture()[0]

    cdir = os.path.dirname(__file__)
    PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
    packages_dir = os.path.dirname(os.path.dirname(sys.executable))
    os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
    os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')

    # Dependencies are automatically detected, but it might need
    # fine tuning.
    buildOptions = {"packages": [],
                    "excludes": ["aiohttp",
                                 "curses",
                                 "distutils",
                                 "lib2to3",
                                 "setuptools"],
                    "includes": ["tkinter",
                                 "numpy.core._methods",
                                 "numpy.lib.format",
                                 "osuapi"],  # idk why this is necessary but
                    # it crashes when i take it out
                    'include_files': [
                        os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'),
                        os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll'),
                        os.path.join(site.getsitepackages()[1], "scipy"),
                        os.path.join(cdir, "data")
                    ],
                    "silent": True
                    }

    if arch == "32bit":
        buildOptions["packages"].append("idna")

    base = 'Win32GUI'

    executables = [
        Executable(os.path.join(cdir, 'blimsu.py'), base=base, targetName='blimsu!.exe'),
        Executable(os.path.join(cdir, 'install.py'), base=base, targetName='install.exe')
    ]

    setup(name='blimsu!',
          version='1.0',
          description='Better Osu Score Summary',
          options=dict(build_exe=buildOptions),
          executables=executables)


if __name__ == "__main__":
    main()
