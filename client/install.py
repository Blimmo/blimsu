import os
import subprocess
import sys

dirPath = os.path.dirname(sys.executable)
exePath = dirPath + "/blimsu!.exe"
command = 'reg add "HKEY_CLASSES_ROOT\\osu!\\shell\\Open with blimsu!\\command" /ve /d "\\"{}\\" \\"%1\\"" /f'
r = subprocess.run(command.format(exePath))
if r.returncode != 0:
    print("Error")
    print(r.stdout)
    print(r.stderr)
    input()
try:
    os.symlink(r".\bin\blimsu!.exe", r"..\blimsu!.exe")
except FileExistsError:
    pass
