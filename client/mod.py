from slider.mod import Mod

modnames = {Mod.hidden: "HD",
            Mod.hard_rock: "HR",
            Mod.easy: "EZ",
            Mod.half_time: "HT",
            Mod.double_time: "DT",
            Mod.nightcore: "NC",
            Mod.flashlight: "FL",
            Mod.no_fail: "NF",
            Mod.spun_out: "SO",
            Mod.sudden_death: "SD",
            Mod.perfect: "PF",
            Mod.relax: "RX",
            Mod.auto_pilot: "AP",
            Mod.autoplay: "AT",
            Mod.key1: "1K",
            Mod.key2: "2K",
            Mod.key3: "3K",
            Mod.key4: "4K",
            Mod.key5: "5K",
            Mod.key6: "6K",
            Mod.key7: "7K",
            Mod.key8: "8K",
            Mod.key9: "9K",
            Mod.coop: "10K",
            Mod.fade_in: "FI",
            Mod.random: "RD",
            Mod.scoreV2: "V2",
            Mod.no_video: "",
            Mod.target_practice: ""
            }


def mods(r):
    for mod in modnames.keys():
        if mod.name != "relax2" and getattr(r, mod.name):
            yield mod
